#include <types.h>
#include <kern/errno.h>
#include <kern/fcntl.h>
#include <kern/limits.h>
#include <kern/stat.h>
#include <kern/seek.h>
#include <lib.h>
#include <uio.h>
#include <thread.h>
#include <current.h>
#include <synch.h>
#include <vfs.h>
#include <vnode.h>
#include <file.h>
#include <proc.h>

/*
 * Add your file-related functions here ...
 */

#define COMMON_CHECK_FD(FD,RES) \
    do{\
    if(FD >= OPEN_MAX || FD < 0)\
        {RES = EBADF;break;}\
    if(curproc->fdesctbl[FD] == NULL)\
        {RES = EBADF;break;}\
    if(curproc->fdesctbl[FD]->glbindex >= OPEN_MAX || curproc->fdesctbl[FD]->glbindex < 0)\
        {RES = EBADF;break;}\
    if(gfiletable[curproc->fdesctbl[FD]->glbindex] == NULL)\
        {RES = EBADF;break;}\
    }while(0)
static struct _ofnode* gfiletable[OPEN_MAX];
static struct lock* gftlock = NULL;

void initglobalfiletable()
{
    gftlock = lock_create("GBL_FLE_TBL");
    for(int i = 0;i < OPEN_MAX;i++)
    {
        gfiletable[i] = NULL;
    }
}

int initstdio()
{
    int inret;
    int outret;
    int errret;
    char* inpath = kstrdup("con:");
    char* outpath = kstrdup("con:");
    char* errpath = kstrdup("con:");
    //open stdin
    int result = sys__open((userptr_t)inpath,O_RDONLY,0,&inret);
    if(result)
    {
        kfree(inpath);
        kfree(outpath);
        kfree(errpath);
        return result;
    }
    
    //open stdout
    result = sys__open((userptr_t)outpath,O_WRONLY,0,&outret);
    if(result)
    {
        sys__close(inret,&inret);
        kfree(inpath);
        kfree(outpath);
        kfree(errpath);
        return result;
    }

    //open stderr
    result = sys__open((userptr_t)errpath,O_WRONLY,0,&errret);
    if(result)
    {
        sys__close(inret,&inret);
        sys__close(outret,&outret);
        kfree(inpath);
        kfree(outpath);
        kfree(errpath);
        return result;
    }
    kfree(inpath);
    kfree(outpath);
    kfree(errpath);
    return 0;
}

int sys__open(userptr_t fname,int flag,mode_t mode,int32_t* ret)
{
    char* path = (char*)fname;
    int id = 0;
    int gid = 0;
    int result = 0;

    while(curproc->fdesctbl[id] != NULL && id < OPEN_MAX)
        id ++;

    if(id == OPEN_MAX)
        return EMFILE;

    //init global file table first
    lock_acquire(gftlock);
    while(gfiletable[gid] != NULL && id < OPEN_MAX)
        gid ++;
    if(gid == OPEN_MAX)
    {
        lock_release(gftlock);
        return ENFILE;
    }
    gfiletable[gid] = (struct _ofnode*)kmalloc(sizeof(struct _ofnode));
    lock_release(gftlock);

    gfiletable[gid]->refernum = 1;
    gfiletable[gid]->flag = flag;
    gfiletable[gid]->offset = 0;
    result = vfs_open(path,flag,mode,&(gfiletable[gid]->p_vnode));
    if(result)
    {
        kfree(gfiletable[gid]);
        gfiletable[gid] = NULL;
        return result;
    }
    //init process file table
    curproc->fdesctbl[id] = (struct _fdnode*)kmalloc(sizeof(struct _fdnode));
    curproc->fdesctbl[id]->glbindex = gid;
    *ret = id;
    return 0; 
}

int sys__close(int fd,int* ret)
{
    int result = 0;
    *ret = -1;
    COMMON_CHECK_FD(fd,result);
    if(result)
        return result;

    //clean global table first
    int gid = curproc->fdesctbl[fd]->glbindex;
    lock_acquire(gftlock);
    gfiletable[gid]->refernum --;
    if(gfiletable[gid]->refernum == 0)
    {
        vfs_close(gfiletable[gid]->p_vnode);
        kfree(gfiletable[gid]);
        gfiletable[gid] = NULL;
    }
    lock_release(gftlock);

    //clean process file table
    kfree(curproc->fdesctbl[fd]);
    curproc->fdesctbl[fd] = NULL;

    result = 0;
    *ret = 0;

    return result;
}

int sys__write(int fd,const void* buf,size_t nbytes,int32_t* ret)
{
    struct iovec iov;
    struct uio ku;
    int result = 0;
    *(size_t*) ret = (size_t)-1;
    COMMON_CHECK_FD(fd,result);
    if(result)
        return result;
    int gid = curproc->fdesctbl[fd]->glbindex;
    if(gfiletable[gid]->flag == O_RDONLY)
        return EBADF;

    //copy userland buffer to kernel 
	iov.iov_ubase = (userptr_t)buf;
	iov.iov_len = nbytes;
	ku.uio_iov = &iov;
	ku.uio_iovcnt = 1;
	ku.uio_offset = gfiletable[gid]->offset;
	ku.uio_resid = nbytes;
	ku.uio_segflg = UIO_USERSPACE;
	ku.uio_rw = UIO_WRITE;
	ku.uio_space = curproc->p_addrspace;
    result = VOP_WRITE(gfiletable[gid]->p_vnode,&ku);
    if(result)
    {
        return EBADF;
    }
    *(size_t*) ret = ku.uio_offset - gfiletable[gid]->offset;
    gfiletable[gid]->offset = ku.uio_offset;
    return result;
}
int sys__read(int fd,void* buf,size_t nbytes,int32_t* ret)
{
    int result = 0;
    *ret = -1;
    struct iovec iov;
    struct uio ku;
    COMMON_CHECK_FD(fd,result);
    if(result)
        return result;
    int gid = curproc->fdesctbl[fd]->glbindex;
    if(gfiletable[gid]->flag & O_WRONLY)
        return EBADF;

    //copy kernel buffer to userland
	iov.iov_ubase = (userptr_t)buf;
	iov.iov_len = nbytes;
	ku.uio_iov = &iov;
	ku.uio_iovcnt = 1;
	ku.uio_offset = gfiletable[gid]->offset;
	ku.uio_resid = nbytes;
	ku.uio_segflg = UIO_USERSPACE;
	ku.uio_rw = UIO_READ;
	ku.uio_space = curproc->p_addrspace;
    result = VOP_READ(gfiletable[gid]->p_vnode,&ku);
    if(result)
    {
        return EBADF;
    }
    *(size_t*) ret = ku.uio_offset - gfiletable[gid]->offset;
	gfiletable[gid]->offset = ku.uio_offset;

    return result;
}

int sys__lseek(int fd,off_t pos,int whence,int32_t* ret,int32_t* ret1)
{
    struct stat nstat;
    int result = 0;
    off_t newpos = 0;
    off_t st_size = 0;
    int gid = -1;
    *ret = -1;
    *ret1 = -1;

    COMMON_CHECK_FD(fd,result);
    if(result)
       return EBADF;

    //check if can seek
    gid = curproc->fdesctbl[fd]->glbindex;
    if(!VOP_ISSEEKABLE(gfiletable[gid]->p_vnode))
        return ESPIPE;

    //get file size and seek
    VOP_STAT(gfiletable[gid]->p_vnode,&nstat);
	st_size = nstat.st_size;		
    switch(whence)
    {
        case SEEK_SET:
            newpos = pos;
            break;
        case SEEK_CUR:
            newpos = gfiletable[gid]->offset + pos;
            break;
        case SEEK_END:
            newpos = st_size + pos;
            break;
        default:
            return EINVAL;
    }
    if(newpos > st_size || newpos < 0)
        return EINVAL;

    gfiletable[gid]->offset = newpos;
    *ret = (off_t)(newpos >> 32);
    *ret1 = (off_t)(newpos & 0xFFFFFFFF);

    return 0;
}

int sys__dup2(int oldfd,int newfd,int32_t* ret)
{
    int result = 0;
    *ret = -1;
    //check 
    COMMON_CHECK_FD(oldfd,result);
    if(result)
        return EBADF;
    if(newfd < 0 ||newfd > OPEN_MAX)
        return EBADF;
    if(newfd == oldfd)
    {
        *ret = newfd; 
        return 0;
    } 
    //check if it was opened
    COMMON_CHECK_FD(newfd,result);
    if(!result)
        sys__close(newfd,&result);

    //two filedescripter will use one global file descripter,referencenum + 1
    lock_acquire(gftlock);
    int gid = curproc->fdesctbl[oldfd]->glbindex;
    gfiletable[gid]->refernum++;
    lock_release(gftlock);
    curproc->fdesctbl[newfd] = (struct _fdnode*)kmalloc(sizeof(struct _fdnode));
    curproc->fdesctbl[newfd]->glbindex = curproc->fdesctbl[oldfd]->glbindex;
    *ret = newfd; 
    return 0;
}
