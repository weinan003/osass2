/*
 * Declarations for file handle and file table management.
 */

#ifndef _FILE_H_
#define _FILE_H_

/*
 * Contains some file-related maximum length constants
 */
#include <limits.h>
#include <types.h>


/*
 * Put your function declarations and data types here ...
 */
struct vnode;
struct _ofnode
{
    struct vnode* p_vnode;
    int refernum;
    int flag;
    off_t offset;
};
struct _fdnode
{
    int glbindex;
};

//it will be invoked by vfslist
void initglobalfiletable(void);
//it is invoked by runprogram every new process created
int initstdio(void);
//main partition of this assignment
int sys__open(userptr_t fname,int flag,mode_t mode,int32_t* ret);
int sys__write(int fd,const void* buf,size_t nbytes,int32_t* ret);
int sys__read(int fd,void* buf,size_t nbytes,int32_t* ret);
int sys__lseek(int fd,off_t pos,int whence,int32_t* ret,int32_t* ret1);
int sys__dup2(int oldfd,int newfd,int32_t* ret);
int sys__close(int fd,int32_t* ret);


#endif /* _FILE_H_ */
